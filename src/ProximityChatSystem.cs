﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;

namespace proximitychat.src
{
    public class ProximityChatSystem : ModSystem
    {
        private ICoreServerAPI sapi;
        private ModConfig config;
        private const string PROXIMITYGROUPNAME = "Proximity";
        private const string CONFIGNAME = "proximitychat.json";

        private PlayerGroup proximityGroup = null;

        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return forSide == EnumAppSide.Server;
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            this.sapi = api;

            try
            {
                this.config = api.LoadModConfig<ModConfig>(CONFIGNAME);
            }
            catch (Exception e)
            {
                api.Server.LogError("proximitychat: Failed to load mod config!");
                return;
            }

            if (this.config == null)
            {
                api.Server.LogNotification($"proximitychat: non-existant modconfig at 'ModConfig/{CONFIGNAME}', creating default...");
                this.config = new ModConfig();
                api.StoreModConfig(this.config, CONFIGNAME);
            }
            else if (this.config.BlockRange <= 0)
            {
                api.Server.LogError($"proximitychat: invalid modconfig at 'ModConfig/{CONFIGNAME}'!");
                return;
            }

            sapi.Event.PlayerChat += Event_PlayerChat;
            sapi.Event.PlayerJoin += Event_PlayerJoin;

            sapi.RegisterCommand("pmessage", "Sends a message to all players in a specific area", null, OnPMessageHandler, Privilege.announce);

            this.proximityGroup = sapi.Groups.GetPlayerGroupByName(PROXIMITYGROUPNAME);
            if (this.proximityGroup == null)
            {
                this.proximityGroup = new PlayerGroup()
                {
                    Name = PROXIMITYGROUPNAME,
                    OwnerUID = null
                };
                sapi.Groups.AddPlayerGroup(this.proximityGroup);
                this.proximityGroup.Md5Identifier = GameMath.Md5Hash(this.proximityGroup.Uid.ToString() + "null");
            }
        }

        private void OnPMessageHandler(IServerPlayer player, int groupId, CmdArgs args)
        {
            Vec3d spawnpos = sapi.World.DefaultSpawnPosition.XYZ;
            spawnpos.Y = 0;
            Vec3d targetpos = null;
            if( player.Entity == null )
            {
                targetpos = args.PopFlexiblePos(spawnpos, spawnpos);
            }
            else
            {
                targetpos = args.PopFlexiblePos(player.Entity.Pos.XYZ, spawnpos);
            }

            if (targetpos == null)
            {
                player.SendMessage(groupId, @"Invalid position supplied. Syntax: [coord] [coord] [coord] whereas
                                             [coord] may be ~[decimal] or =[decimal] or [decimal]. 
                                             ~ denotes a position relative to the player 
                                             = denotes an absolute position 
                                             no prefix denotes a position relative to the map middle", EnumChatType.CommandError);
                return;
            }

            var blockRadius = args.PopInt();
            if (!blockRadius.HasValue)
            {
                player.SendMessage(groupId, "Invalid radius supplied. Syntax: =[abscoord] =[abscoord] =[abscoord] [radius]", EnumChatType.CommandError);
                return;
            }

            var message = args.PopAll();
            if(string.IsNullOrEmpty(message))
            {
                player.SendMessage(groupId, "Invalid message supplied. Syntax: =[abscoord] =[abscoord] =[abscoord] [radius] [message]", EnumChatType.CommandError);
                return;
            }

            foreach (var nearbyPlayerData in this.sapi.World.AllOnlinePlayers.Select(x => new { Position = x.Entity.ServerPos, Player = (IServerPlayer) x }).Where(x => Math.Abs( x.Position.DistanceTo(targetpos) ) < blockRadius))
            {
                nearbyPlayerData.Player.SendMessage(this.proximityGroup.Uid, message, EnumChatType.CommandSuccess);
            }
        }

        private void Event_PlayerJoin(IServerPlayer byPlayer)
        {
            var proximityGroup = sapi.Groups.GetPlayerGroupByName(PROXIMITYGROUPNAME);
            var playerProximityGroup = byPlayer.GetGroup(proximityGroup.Uid);
            if (playerProximityGroup == null)
            {
                var newMembership = new PlayerGroupMembership()
                {
                    GroupName = proximityGroup.Name,
                    GroupUid = proximityGroup.Uid,
                    Level = EnumPlayerGroupMemberShip.Member
                };
                byPlayer.ServerData.PlayerGroupMemberShips.Add(proximityGroup.Uid, newMembership);
                proximityGroup.OnlinePlayers.Add(byPlayer);
            }
        }

        private void Event_PlayerChat(IServerPlayer byPlayer, int channelId, ref string message, ref string data, Vintagestory.API.Datastructures.BoolRef consumed)
        {
            var proximityGroup = sapi.Groups.GetPlayerGroupByName(PROXIMITYGROUPNAME);
            if (proximityGroup.Uid == channelId)
            {
                foreach (var player in this.sapi.World.AllOnlinePlayers.Where(x => x.Entity.Pos.AsBlockPos.ManhattenDistance(byPlayer.Entity.Pos.AsBlockPos) < this.config.BlockRange))
                {
                    var serverPlayer = player as IServerPlayer;
                    serverPlayer.SendMessage(proximityGroup.Uid, message, EnumChatType.OthersMessage, data);
                }

                consumed.value = true;
            }
        }
    }
}
